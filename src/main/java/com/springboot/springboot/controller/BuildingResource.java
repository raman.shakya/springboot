package com.springboot.springboot.controller;

import com.springboot.springboot.model.Building;
import com.springboot.springboot.service.BuildingService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(BaseResource.BASE_URL + BuildingResource.BASE_URL)
public class BuildingResource extends BaseResource<Building> {

    static final String BASE_URL = "/buildings";

    public BuildingResource(BuildingService buildingService) {
        super(buildingService);
    }
}
