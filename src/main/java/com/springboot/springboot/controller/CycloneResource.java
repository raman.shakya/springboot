package com.springboot.springboot.controller;

import com.springboot.springboot.model.Cyclone;
import com.springboot.springboot.service.CycloneService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(BaseResource.BASE_URL + CycloneResource.BASE_URL)
public class CycloneResource extends BaseResource<Cyclone> {

    static final String BASE_URL = "/cyclones";

    public CycloneResource(CycloneService cycloneService) {
        super(cycloneService);
    }
}
