package com.springboot.springboot.controller;

import com.springboot.springboot.model.BaseEntity;
import com.springboot.springboot.service.BaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class BaseResource<T extends BaseEntity> {

    static final String BASE_URL = "/api";

    private final BaseService<T> service;

    public BaseResource(BaseService<T> service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<T>> findAll() {
        return ResponseEntity.ok().body(service.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<T>> findById(@PathVariable Long id) {
        return ResponseEntity.ok().body(service.findById(id));
    }

    @PostMapping
    public ResponseEntity<T> save(@RequestBody T t) {
        return ResponseEntity.ok().body(service.save(t));
    }

}
