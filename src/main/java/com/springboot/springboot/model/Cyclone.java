package com.springboot.springboot.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Cyclone extends BaseEntity {

    private String name;
    private double contact;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Building> buildings;

    public Cyclone() {
    }

    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "Contact")
    public double getContact() {
        return contact;
    }

    public void setContact(double contact) {
        this.contact = contact;
    }

    public List<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }

    @Override
    public String toString() {
        return "Cyclone{" +
                "name='" + name + '\'' +
                ", contact=" + contact +
                ", buildings=" + buildings +
                '}';
    }
}
