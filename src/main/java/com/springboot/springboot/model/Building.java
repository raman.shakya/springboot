package com.springboot.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Building extends BaseEntity {

    private String address;
    private String type;

    public Building() {
    }

    @Column(name = "Address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "Type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Building{" +
                "address='" + address + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
