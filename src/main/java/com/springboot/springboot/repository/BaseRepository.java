package com.springboot.springboot.repository;

import com.springboot.springboot.model.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseRepository<T extends BaseEntity, N extends Number> extends JpaRepository<T, N> {
}
