package com.springboot.springboot.repository;

import com.springboot.springboot.model.Building;

public interface BuildingRepository extends BaseRepository<Building, Long> {
}