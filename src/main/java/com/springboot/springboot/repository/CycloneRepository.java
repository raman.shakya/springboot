package com.springboot.springboot.repository;

import com.springboot.springboot.model.Cyclone;

public interface CycloneRepository extends BaseRepository<Cyclone, Long> {
}
