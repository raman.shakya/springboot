package com.springboot.springboot.service.impl;

import com.springboot.springboot.model.Building;
import com.springboot.springboot.repository.BaseRepository;
import com.springboot.springboot.repository.BuildingRepository;
import com.springboot.springboot.service.BuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuildingServiceImpl extends BaseServiceImpl<Building> implements BuildingService {

    @Autowired
    private BuildingRepository buildingRepository;

    public BuildingServiceImpl(BaseRepository<Building, Number> buildingNumberBaseRepository) {
        super(buildingNumberBaseRepository);
    }

    @Override
    public List<Building> findByAddress(String address) {
        return buildingRepository.findAll();
    }
}
