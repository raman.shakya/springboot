package com.springboot.springboot.service.impl;

import com.springboot.springboot.model.Cyclone;
import com.springboot.springboot.repository.CycloneRepository;
import com.springboot.springboot.service.CycloneService;
import org.springframework.stereotype.Service;


@Service
public class CycloneServiceImpl extends BaseServiceImpl<Cyclone> implements CycloneService {

    private final CycloneRepository cycloneRepository;

    public CycloneServiceImpl(CycloneRepository cycloneRepository) {
        super(cycloneRepository);
        this.cycloneRepository = cycloneRepository;
    }
}
