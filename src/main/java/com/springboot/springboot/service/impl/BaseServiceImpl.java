package com.springboot.springboot.service.impl;

import com.springboot.springboot.model.BaseEntity;
import com.springboot.springboot.repository.BaseRepository;
import com.springboot.springboot.service.BaseService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BaseServiceImpl<T extends BaseEntity> implements BaseService<T> {

    private BaseRepository baseRepository;

    public BaseServiceImpl(BaseRepository baseRepository) {
        this.baseRepository = baseRepository;
    }

    @Override
    public List<T> findAll() {
        return baseRepository.findAll();
    }

    @Override
    public Optional<T> findById(Long id) {
        return baseRepository.findById(id);
    }

    @Override
    public T save(T t) {
        return null;
    }

    @Override
    public void delete(T t) {
        baseRepository.delete(t);
    }


}
