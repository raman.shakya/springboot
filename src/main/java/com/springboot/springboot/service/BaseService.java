package com.springboot.springboot.service;

import com.springboot.springboot.model.BaseEntity;

import java.util.List;
import java.util.Optional;

public interface BaseService<T extends BaseEntity> {

    List<T> findAll();

    Optional<T> findById(Long id);

    T save(T t);

    void delete(T t);

}
