package com.springboot.springboot.service;

import com.springboot.springboot.model.Building;

import java.util.List;

public interface BuildingService extends BaseService<Building> {
    List<Building> findByAddress(String address);
}
